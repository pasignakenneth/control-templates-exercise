﻿using System;
using System.Collections.Generic;
using System.Windows.Input;
using Xamarin.Forms;

namespace ControlTemplatesExercise
{
    public partial class ViewTwoPage : ContentPage
    {
        public static readonly BindableProperty AddIconProperty = BindableProperty.Create("AddIcon", typeof(string), typeof(ViewTwoPage), null);
        public static readonly BindableProperty AddIconCommandProperty = BindableProperty.Create("AddIcon_Command", typeof(ICommand), typeof(ViewTwoPage), null);
        public ViewTwoPage()
        {
            InitializeComponent();
            NavigationPage.SetHasNavigationBar(this, false);
            this.AddIcon = "cloud.png";
            this.AddIcon_Command = new Command((obj) => DisplayAlert("Cloud", "Blah", "Okay"));
        }
        public string BackButton
        {
            get
            {
                return "http://cdn.onlinewebfonts.com/svg/img_170454.png";
            }
        }
        public string HeaderText
        {
            get
            {
                return "View 2";
            }
        }
        public string AddIcon
        {
            get
            {
                return (string)GetValue(AddIconProperty);
            }
            set
            {
                SetValue(AddIconProperty, value);
            }
        }
        public ICommand AddIcon_Command
        {
            get
            {
                return (ICommand)GetValue(AddIconCommandProperty);
            }
            set
            {
                SetValue(AddIconCommandProperty, value);
            }
        }
    }
}
