﻿using System;
using System.Collections.Generic;
using System.Windows.Input;
using Xamarin.Forms;

namespace ControlTemplatesExercise
{
    public partial class ListPage : ContentPage
    {
        public static readonly BindableProperty AddIconProperty = BindableProperty.Create("AddIcon", typeof(string), typeof(ListPage), null);
        public static readonly BindableProperty AddIconCommandProperty = BindableProperty.Create("AddIcon_Command", typeof(ICommand), typeof(ListPage), null);

        public ListPage()
        {
            InitializeComponent();
            NavigationPage.SetHasNavigationBar(this, false);
            this.AddIcon_Command = new Command((obj) => DisplayAlert("Add Icon", "Blah", "Okay"));
            this.AddIcon = "add.png";
        }
        public string BackButton
        {
            get
            {
                return "http://cdn.onlinewebfonts.com/svg/img_170454.png";
            }
        }
        public string HeaderText
        {
            get
            {
                return "List";
            }
        }
        public string AddIcon
        {
            get
            {
                return (string)GetValue (AddIconProperty);
            }
            set
            {
                SetValue(AddIconProperty, value);
            }
        }
        public ICommand AddIcon_Command
        {
            get
            {
                return (ICommand)GetValue(AddIconCommandProperty);
            }
            set
            {
                SetValue(AddIconCommandProperty, value);
            }

        }

    }
}
