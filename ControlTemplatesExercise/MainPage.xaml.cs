﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace ControlTemplatesExercise
{
    public partial class MainPage : ContentPage
    {
        public static readonly BindableProperty HeaderTextProperty = BindableProperty.Create("HeaderText", typeof(string), typeof(MainPage), "MAIN");

        public MainPage()
        {
            InitializeComponent();
            NavigationPage.SetHasNavigationBar(this, false);
        }
        public string HeaderText
        {
            get
            {
                return (string)GetValue (HeaderTextProperty);
            }
        }
        void OnBtnClicked_List()
        {
            Navigation.PushAsync(new ListPage());

        }
        void OnBtnClicked_View1()
        {
            Navigation.PushAsync(new ViewOnePage());
        }
        void OnBtnClicked_View2()
        {
            Navigation.PushAsync(new ViewTwoPage());
        }
        void OnBtnClicked_View3()
        {
            Navigation.PushModalAsync(new ViewThreePage());
        }
    }
}
