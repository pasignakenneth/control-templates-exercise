﻿using System;
using System.Collections.Generic;
using System.Windows.Input;
using Xamarin.Forms;

namespace ControlTemplatesExercise
{
    public partial class ViewThreePage : ContentPage
    {   
        public static readonly BindableProperty AddIconProperty = BindableProperty.Create("AddIcon", typeof(string), typeof(ViewThreePage), null);
        public static readonly BindableProperty AddIconCommandProperty = BindableProperty.Create("AddIcon_Command", typeof(ICommand), typeof(ViewThreePage), null);
        
        public ViewThreePage()
        {
            InitializeComponent();
            NavigationPage.SetHasNavigationBar(this, false);
            this.AddIcon = "close.png";
            this.AddIcon_Command = new Command((obj) => Navigation.PopModalAsync());
        }

        public string HeaderText
        {
            get
            {
                return "View 3";
            }
        }
        public string AddIcon
        {
            get
            {
                return (string)GetValue(AddIconProperty);
            }
            set
            {
                SetValue(AddIconProperty, value);
            }
        }
        public ICommand AddIcon_Command
        {
            get
            {
                return (ICommand)GetValue(AddIconCommandProperty);
            }
            set
            {
                SetValue(AddIconCommandProperty, value);
            }
        }
    }
}
