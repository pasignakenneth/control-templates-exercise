using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

[assembly: XamlCompilation(XamlCompilationOptions.Compile)]
namespace ControlTemplatesExercise
{
    public partial class App : Application
    {
        public App()
        {
            InitializeComponent();

            MainPage = new NavigationPage(new MainPage());

        }
        void BackButton_Tapped(object sender, System.EventArgs e)
        {
            MainPage.Navigation.PopAsync();
            System.Diagnostics.Debug.WriteLine(sender.GetHashCode());

        }
        void EditIcon_Tapped(object sender, EventArgs e)
        {
            MainPage.DisplayAlert("Edit Icon", "Blah", "Okay");
            System.Diagnostics.Debug.WriteLine(sender.GetHashCode());

        }



        protected override void OnStart()
        {
            // Handle when your app starts
        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
        }

        protected override void OnResume()
        {
            // Handle when your app resumes
        }

    }
}
