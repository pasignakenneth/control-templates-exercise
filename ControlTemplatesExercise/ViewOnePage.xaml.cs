﻿using System;
using System.Collections.Generic;
using System.Windows.Input;
using Xamarin.Forms;

namespace ControlTemplatesExercise
{
    public partial class ViewOnePage : ContentPage
    {
        public static readonly BindableProperty AddIconProperty = BindableProperty.Create("AddIcon", typeof(string), typeof(ViewOnePage), null);
        public static readonly BindableProperty AddIconCommandProperty = BindableProperty.Create("AddIcon_Command", typeof(ICommand), typeof(ViewOnePage), null);
        public static readonly BindableProperty EditIconProperty = BindableProperty.Create("EditIcon", typeof(string), typeof(ViewOnePage), null);
        public static readonly BindableProperty EditIconCommandProperty = BindableProperty.Create("EditIcon_Command", typeof(ICommand), typeof(ViewOnePage), null);


        public ViewOnePage()
        {
            InitializeComponent();
            NavigationPage.SetHasNavigationBar(this, false);
            this.AddIcon = "delete.png";
            this.AddIcon_Command = new Command((obj) => DisplayAlert("Delete", "Blah", "Okay"));
            this.EditIcon = "edit.png";
            this.EditIcon_Command = new Command((obj) => DisplayAlert("Edit Icon", "Blah", "Okay"));
        }
        public string BackButton
        {
            get
            {
                return "http://cdn.onlinewebfonts.com/svg/img_170454.png";
            }
        }
        public string HeaderText
        {
            get
            {
                return "View 1";
            }
        }

        public string AddIcon
        {
            get
            {
                return (string)GetValue(AddIconProperty);
            }
            set
            {
                SetValue(AddIconProperty, value);
            }
        }
        public ICommand AddIcon_Command
        {
            get
            {
                return (ICommand)GetValue(AddIconCommandProperty);
            }
            set
            {
                SetValue(AddIconCommandProperty, value);
            }
        }
        public string EditIcon
        {
            get
            {
                return (string)GetValue(EditIconProperty);
            }
            set
            {
                SetValue(EditIconProperty, value);
            }
        }
        public ICommand EditIcon_Command
        {
            get
            {
                return (ICommand)GetValue(EditIconCommandProperty);
            }
            set
            {
                SetValue(EditIconCommandProperty, value);
            }

        }
    }
}
